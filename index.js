const express = require('express');
const fetch = require('node-fetch');
const redis = require('redis');

const PORT = process.env.PORT || 5000;
const REDIS_PORT = process.env.REDIS_PORT || 6379;

const client = redis.createClient(REDIS_PORT);

const app = express();

// Set response
function setResponse(locale, language) {
  return `<h2>API ${locale} language has response</h2><p>${language}</p>`;
}

// Make request to API for data
async function getRepos(req, res, next) {
  try {
    console.log('Fetching Data...');

    const { locale } = req.params;

    const response = await fetch(`https://cms.dch.com.hk/api/v1.0/page-content?locale=${locale}`);

    const data = await response.json();

    const language = JSON.stringify(data.common);

    // Set data to Redis
    client.setex(locale, 3600, language);

    res.send(setResponse(locale, language));
  } catch (err) {
    console.error(err);
    res.status(500);
  }
}

// Cache middleware
function cache(req, res, next) {
  const { locale } = req.params;

  client.get(locale, (err, data) => {
    if (err) throw err;

    if (data !== null) {
      res.send(setResponse(locale, data));
    } else {
      next();
    }
  });
}

app.get('/api/:locale', cache, getRepos);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
